//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: ansiShadow
// An ANSI Block Text Generator
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "ansiShadow_cmdline.h"
#include "unicodeChars.h"
#include "dosChars.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define TERM						"TERM"
#define MSYSCON						"MSYSCON"
#define NUMBERofLINES				7
#define SETBACKGROUNDCOLORMODE		"\033[4%dm"
#define RESETBACKGROUNDCOLORMODE	"\033[0m"

#define error(fmt, ...)				do { fprintf(stderr, "ERROR: " fmt "\n", __VA_ARGS__); } while (0)
// Example: error("Error at line=%d", n);
//			error("%s", "No valide argument");
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
int ssgetTerminalType() {
	char *pt;

	if (NULL == getenv(TERM)) return(1);				// Windows Command Line
	if ((pt = getenv(MSYSCON)) == NULL) return(0);		// Linux shell
	if (strcmp(pt, "sh.exe") == 0) return(1);			// Windows/Bash shell
	return(0);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	const char *_colors[8] = {
		"BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE"
	};
	char mask[8] = "";
	char unmask[8] = "";

	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_ansiShadow(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Unicode or Dos output ? ---------------------------------------------
	int windowsterm = -1;
	if (args_info.unicode_given) windowsterm = 0;
	else if (args_info.dos_given) windowsterm = 1;
	else windowsterm = ssgetTerminalType();

	if (args_info.background_given) {
		char *color = (char *)calloc(strlen(args_info.background_arg), sizeof(char));
		if (color == NULL) {
			error("Cannot allocate memory (%ld bytes)...", strlen(args_info.background_arg));
			goto EXIT;
		}
		int i;
		for (i = 0; args_info.background_arg[i]; i++) {
			color[i] = (char)toupper(args_info.background_arg[i]);
		}
		for (i = 0; i < 8; i++) {
			if (strcmp(_colors[i], color) == 0) break;
		}
		free(color);
		if (i == 8) {
			error("Bad color ('%s').", args_info.background_arg);
			goto EXIT;
		}
		sprintf(mask, SETBACKGROUNDCOLORMODE, i);
		strcpy(unmask, RESETBACKGROUNDCOLORMODE);
	}
	//---- Go on ---------------------------------------------------------------
	fprintf(stdout, "\n");
	if (windowsterm) {
		long unsigned int *shiftings = (long unsigned int *)calloc(strlen(args_info.string_arg), sizeof(long unsigned int));
		if (shiftings == NULL) {
			error("Cannot allocate memory (%ld bytes)...", strlen(args_info.string_arg));
			goto EXIT;
		}
		for (long unsigned int line = 0; line < NUMBERofLINES; line++) {
			fprintf(stdout, "%s ", mask);
			for (unsigned int i = 0; i < strlen(args_info.string_arg); i++) {
				int idx = toupper(args_info.string_arg[i]);
				if (idx > 95) idx = ' ';
				idx = idx  - ' ';
				long unsigned int k = shiftings[i];
				while (_dos[idx][k] != 0) fprintf(stdout, "%c", _dos[idx][k++]);
				shiftings[i] = k + 1;
			}
			fprintf(stdout, " %s\n", unmask);
		}
		free(shiftings);
	} else {
		for (int line = 0; line < NUMBERofLINES; line++) {
			fprintf(stdout, "%s ", mask);
			for (unsigned int i = 0; i < strlen(args_info.string_arg); i++) {
				int idx = toupper(args_info.string_arg[i]);
				if (idx > 95) idx = ' ';
				fprintf(stdout, "%s", _unicode[idx - ' '][line]);
			}
			fprintf(stdout, " %s\n", unmask);
		}
	}
	if (! windowsterm) fprintf(stdout, "\n");
	//---- Exit ----------------------------------------------------------------
	cmdline_parser_ansiShadow_free(&args_info);
	return EXIT_SUCCESS;
EXIT:
	cmdline_parser_ansiShadow_free(&args_info);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
// Font Author: ?
// More Info:
// https://web.archive.org/web/20120819044459/http://www.roysac.com/thedrawfonts-tdf.asp
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: ansiShadow
// An ANSI Block Text Generator
//------------------------------------------------------------------------------
#ifndef UNICODECHARS_H
#define UNICODECHARS_H
//------------------------------------------------------------------------------
// BLOCK CHAR  DEFINITIONS
//------------------------------------------------------------------------------
static const char *_unicodeSP[7] = { "    ",
                                     "    ",
                                     "    ",
                                     "    ",
                                     "    ",
                                     "    ",
                                     "    "
                                   };

static const char *_unicode0[7] = { "         ",
                                    " ██████╗ ",
                                    "██╔═████╗",
                                    "██║██╔██║",
                                    "████╔╝██║",
                                    "╚██████╔╝",
                                    " ╚═════╝ "
                                  };

static const char *_unicode1[7] = { "    ",
                                    " ██╗",
                                    "███║",
                                    "╚██║",
                                    " ██║",
                                    " ██║",
                                    " ╚═╝"
                                  };

static const char *_unicode2[7] = { "        ",
                                    "██████╗ ",
                                    "╚════██╗",
                                    " █████╔╝",
                                    "██╔═══╝ ",
                                    "███████╗",
                                    "╚══════╝"
                                  };

static const char *_unicode3[7] = { "        ",
                                    "██████╗ ",
                                    "╚════██╗",
                                    " █████╔╝",
                                    " ╚═══██╗",
                                    "██████╔╝",
                                    "╚═════╝ "
                                  };

static const char *_unicode4[7] = { "        ",
                                    "██╗  ██╗",
                                    "██║  ██║",
                                    "███████║",
                                    "╚════██║",
                                    "     ██║",
                                    "     ╚═╝"
                                  };

static const char *_unicode5[7] = { "        ",
                                    "███████╗",
                                    "██╔════╝",
                                    "███████╗",
                                    "╚════██║",
                                    "███████║",
                                    "╚══════╝"
                                  };

static const char *_unicode6[7] = { "        ",
                                    " ██████╗ ",
                                    "██╔════╝ ",
                                    "███████╗ ",
                                    "██╔═══██╗",
                                    "╚██████╔╝",
                                    " ╚═════╝ "
                                  };

static const char *_unicode7[7] = { "        ",
                                    "███████╗",
                                    "╚════██║",
                                    "    ██╔╝",
                                    "   ██╔╝ ",
                                    "   ██║  ",
                                    "   ╚═╝  "
                                  };

static const char *_unicode8[7] = { "        ",
                                    " █████╗ ",
                                    "██╔══██╗",
                                    "╚█████╔╝",
                                    "██╔══██╗",
                                    "╚█████╔╝",
                                    " ╚════╝ "
                                  };

static const char *_unicode9[7] = { "        ",
                                    " █████╗ ",
                                    "██╔══██╗",
                                    "╚██████║",
                                    " ╚═══██║",
                                    " █████╔╝",
                                    " ╚════╝ "
                                  };

static const char *_unicodeA[7] = { "        ",
                                    " █████╗ ",
                                    "██╔══██╗",
                                    "███████║",
                                    "██╔══██║",
                                    "██║  ██║",
                                    "╚═╝  ╚═╝"
                                  };

static const char *_unicodeB[7] = { "        ",
                                    "██████╗ ",
                                    "██╔══██╗",
                                    "██████╔╝",
                                    "██╔══██╗",
                                    "██████╔╝",
                                    "╚═════╝ "
                                  };

static const char *_unicodeC[7] = { "        ",
                                    " ██████╗",
                                    "██╔════╝",
                                    "██║     ",
                                    "██║     ",
                                    "╚██████╗",
                                    " ╚═════╝"
                                  };

static const char *_unicodeD[7] = { "        ",
                                    "██████╗ ",
                                    "██╔══██╗",
                                    "██║  ██║",
                                    "██║  ██║",
                                    "██████╔╝",
                                    "╚═════╝ "
                                  };

static const char *_unicodeE[7] = { "        ",
                                    "███████╗",
                                    "██╔════╝",
                                    "█████╗  ",
                                    "██╔══╝  ",
                                    "███████╗",
                                    "╚══════╝"
                                  };

static const char *_unicodeF[7] = { "        ",
                                    "███████╗",
                                    "██╔════╝",
                                    "█████╗  ",
                                    "██╔══╝  ",
                                    "██║     ",
                                    "╚═╝     "
                                  };

static const char *_unicodeG[7] = { "        ",
                                    " ██████╗ ",
                                    "██╔════╝ ",
                                    "██║  ███╗",
                                    "██║   ██║",
                                    "╚██████╔╝",
                                    " ╚═════╝ "
                                  };

static const char *_unicodeH[7] = { "        ",
                                    "██╗  ██╗",
                                    "██║  ██║",
                                    "███████║",
                                    "██╔══██║",
                                    "██║  ██║",
                                    "╚═╝  ╚═╝"
                                  };

static const char *_unicodeI[7] = { "   ",
                                    "██╗",
                                    "██║",
                                    "██║",
                                    "██║",
                                    "██║",
                                    "╚═╝"
                                  };

static const char *_unicodeJ[7] = { "        ",
                                    "     ██╗",
                                    "     ██║",
                                    "     ██║",
                                    "██   ██║",
                                    "╚█████╔╝",
                                    " ╚════╝ "
                                  };

static const char *_unicodeK[7] = { "        ",
                                    "██╗  ██╗",
                                    "██║ ██╔╝",
                                    "█████╔╝ ",
                                    "██╔═██╗ ",
                                    "██║  ██╗",
                                    "╚═╝  ╚═╝"
                                  };

static const char *_unicodeL[7] = { "        ",
                                    "██╗     ",
                                    "██║     ",
                                    "██║     ",
                                    "██║     ",
                                    "███████╗",
                                    "╚══════╝"
                                  };

static const char *_unicodeM[7] = { "           ",
                                    "███╗   ███╗",
                                    "████╗ ████║",
                                    "██╔████╔██║",
                                    "██║╚██╔╝██║",
                                    "██║ ╚═╝ ██║",
                                    "╚═╝     ╚═╝"
                                  };

static const char *_unicodeN[7] = { "          ",
                                    "███╗   ██╗",
                                    "████╗  ██║",
                                    "██╔██╗ ██║",
                                    "██║╚██╗██║",
                                    "██║ ╚████║",
                                    "╚═╝  ╚═══╝"
                                  };

static const char *_unicodeO[7] = { "         ",
                                    " ██████╗ ",
                                    "██╔═══██╗",
                                    "██║   ██║",
                                    "██║   ██║",
                                    "╚██████╔╝",
                                    " ╚═════╝ "
                                  };

static const char *_unicodeP[7] = { "        ",
                                    "██████╗ ",
                                    "██╔══██╗",
                                    "██████╔╝",
                                    "██╔═══╝ ",
                                    "██║     ",
                                    "╚═╝     "
                                  };

static const char *_unicodeQ[7] = { "         ",
                                    " ██████╗ ",
                                    "██╔═══██╗",
                                    "██║   ██║",
                                    "██║▄▄ ██║",
                                    "╚██████╔╝",
                                    " ╚══▀▀═╝ "
                                  };

static const char *_unicodeR[7] = { "        ",
                                    "██████╗ ",
                                    "██╔══██╗",
                                    "██████╔╝",
                                    "██╔══██╗",
                                    "██║  ██║",
                                    "╚═╝  ╚═╝"
                                  };

static const char *_unicodeS[7] = { "        ",
                                    "███████╗",
                                    "██╔════╝",
                                    "███████╗",
                                    "╚════██║",
                                    "███████║",
                                    "╚══════╝"
                                  };

static const char *_unicodeT[7] = { "         ",
                                    "████████╗",
                                    "╚══██╔══╝",
                                    "   ██║   ",
                                    "   ██║   ",
                                    "   ██║   ",
                                    "   ╚═╝   "
                                  };

static const char *_unicodeU[7] = { "         ",
                                    "██╗   ██╗",
                                    "██║   ██║",
                                    "██║   ██║",
                                    "██║   ██║",
                                    "╚██████╔╝",
                                    " ╚═════╝ "
                                  };

static const char *_unicodeV[7] = { "         ",
                                    "██╗   ██╗",
                                    "██║   ██║",
                                    "██║   ██║",
                                    "╚██╗ ██╔╝",
                                    " ╚████╔╝ ",
                                    "  ╚═══╝  "
                                  };

static const char *_unicodeW[7] = { "          ",
                                    "██╗    ██╗",
                                    "██║    ██║",
                                    "██║ █╗ ██║",
                                    "██║███╗██║",
                                    "╚███╔███╔╝",
                                    " ╚══╝╚══╝ "
                                  };

static const char *_unicodeX[7] = { "        ",
                                    "██╗  ██╗",
                                    "╚██╗██╔╝",
                                    " ╚███╔╝ ",
                                    " ██╔██╗ ",
                                    "██╔╝ ██╗",
                                    "╚═╝  ╚═╝"
                                  };

static const char *_unicodeY[7] = { "         ",
                                    "██╗   ██╗",
                                    "╚██╗ ██╔╝",
                                    " ╚████╔╝ ",
                                    "  ╚██╔╝  ",
                                    "   ██║   ",
                                    "   ╚═╝   "
                                  };

static const char *_unicodeZ[7] = { "        ",
                                    "███████╗",
                                    "╚══███╔╝",
                                    "  ███╔╝ ",
                                    " ███╔╝  ",
                                    "███████╗",
                                    "╚══════╝"
                                  };

static const char *_unicodeAMP[7] = { "         ",
                                      "   ██╗   ",
                                      "   ██║   ",
                                      "████████╗",
                                      "██╔═██╔═╝",
                                      "██████║  ",
                                      "╚═════╝  "
                                    };

static const char *_unicodeSHA[7] = { "         ",
                                      " ██╗ ██╗ ",
                                      "████████╗",
                                      "╚██╔═██╔╝",
                                      "████████╗",
                                      "╚██╔═██╔╝",
                                      " ╚═╝ ╚═╝ "
                                    };

static const char *_unicodeLB[7] = { "    ",
                                     " ██╗",
                                     "██╔╝",
                                     "██║ ",
                                     "██║ ",
                                     "╚██╗",
                                     " ╚═╝"
                                   };

static const char *_unicodeLSB[7] = { "    ",
                                      "███╗",
                                      "██╔╝",
                                      "██║ ",
                                      "██║ ",
                                      "███╗",
                                      "╚══╝"
                                    };

static const char *_unicodeDASH[7] = { "      ",
                                       "      ",
                                       "      ",
                                       "█████╗",
                                       "╚════╝",
                                       "      ",
                                       "      "
                                     };

static const char *_unicodeUNDER[7] = { "        ",
                                        "        ",
                                        "        ",
                                        "        ",
                                        "        ",
                                        "███████╗",
                                        "╚══════╝"
                                      };

static const char *_unicodeCARET[7] = { "      ",
                                        " ███╗ ",
                                        "██╔██╗",
                                        "╚═╝╚═╝",
                                        "      ",
                                        "      ",
                                        "      "
                                      };

static const char *_unicodeAROB[7] = { "         ",
                                       " ██████╗ ",
                                       "██╔═══██╗",
                                       "██║██╗██║",
                                       "██║██║██║",
                                       "╚█║████╔╝",
                                       " ╚╝╚═══╝ "
                                     };

static const char *_unicodeRB[7] = { "    ",
                                     "██╗ ",
                                     "╚██╗",
                                     " ██║",
                                     " ██║",
                                     "██╔╝",
                                     "╚═╝ "
                                   };

static const char *_unicodeRSB[7] = { "    ",
                                      "███╗",
                                      "╚██║",
                                      " ██║",
                                      " ██║",
                                      "███║",
                                      "╚══╝"
                                    };

static const char *_unicodeGT[7] = { "     ",
                                     "██╗  ",
                                     "╚██╗ ",
                                     " ╚██╗",
                                     " ██╔╝",
                                     "██╔╝ ",
                                     "╚═╝  "
                                   };

static const char *_unicodeLT[7] = { "     ",
                                     "  ██╗",
                                     " ██╔╝",
                                     "██╔╝ ",
                                     "╚██╗ ",
                                     " ╚██╗",
                                     "  ╚═╝"
                                   };

static const char *_unicodeQUES[7] = { "         ",
                                       "██████╗  ",
                                       "╚════██╗ ",
                                       "  ▄███╔╝ ",
                                       "  ▀▀══╝  ",
                                       "  ██╗    ",
                                       "  ╚═╝    "
                                     };

static const char *_unicodeCOMMA[7] = { "    ",
                                        "    ",
                                        "    ",
                                        "    ",
                                        "    ",
                                        "▄█╗ ",
                                        "╚═╝ "
                                      };

static const char *_unicodeDOT[7] = { "   ",
                                      "   ",
                                      "   ",
                                      "   ",
                                      "   ",
                                      "██╗",
                                      "╚═╝"
                                    };

static const char *_unicodeSEMICOLON[7] = { "    ",
                                            "    ",
                                            "    ",
                                            "██╗ ",
                                            "╚═╝ ",
                                            "▄█╗ ",
                                            "▀═╝ "
                                          };

static const char *_unicodeSLASH[7] = { "       ",
                                        "    ██╗",
                                        "   ██╔╝",
                                        "  ██╔╝ ",
                                        " ██╔╝  ",
                                        "██╔╝   ",
                                        "╚═╝    "
                                      };

static const char *_unicodeCOLON[7] = { "    ",
                                        "    ",
                                        "██╗ ",
                                        "╚═╝ ",
                                        "██╗ ",
                                        "╚═╝ ",
                                        "    "
                                      };

static const char *_unicodeEXCLAM[7] = { "      "
                                         "  ██╗ ",
                                         "  ██║ ",
                                         "  ██║ ",
                                         "  ╚═╝ ",
                                         "  ██╗ ",
                                         "  ╚═╝ "
                                       };
//" ! #  & ()  ,-./0123456789:;< >?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[ ]^_"
const char **_unicode[64] = {
	_unicodeSP,    _unicodeEXCLAM, _unicodeSP,    _unicodeSHA,       _unicodeSP,   _unicodeSP,
	_unicodeAMP,   _unicodeSP,     _unicodeLB,    _unicodeRB,        _unicodeSP,   _unicodeSP,
	_unicodeCOMMA, _unicodeDASH,   _unicodeDOT,   _unicodeSLASH,     _unicode0,    _unicode1,
	_unicode2,     _unicode3,      _unicode4,     _unicode5,         _unicode6,    _unicode7,
	_unicode8,     _unicode9,      _unicodeCOLON, _unicodeSEMICOLON, _unicodeLT,   _unicodeSP,
	_unicodeGT,    _unicodeQUES,   _unicodeAROB,  _unicodeA,         _unicodeB,    _unicodeC,
	_unicodeD,     _unicodeE,      _unicodeF,     _unicodeG,         _unicodeH,    _unicodeI,
	_unicodeJ,     _unicodeK,      _unicodeL,     _unicodeM,         _unicodeN,    _unicodeO,
	_unicodeP,     _unicodeQ,      _unicodeR,     _unicodeS,         _unicodeT,    _unicodeU,
	_unicodeV,     _unicodeW,      _unicodeX,     _unicodeY,         _unicodeZ,    _unicodeLSB,
	_unicodeSP,    _unicodeRSB,    _unicodeCARET, _unicodeUNDER
};

#endif	// UNICODECHARS_H
//------------------------------------------------------------------------------

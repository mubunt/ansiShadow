# *ansiShadow*, a Text to ANSI Block Character Generator.
***
**ansiShadow** allows 2 types of encodings for the output (aka "banner"):
   - Unicode for Linux banner.
   - Dos (or "code page 437") for Windows banner.
Thus, it is possible to generate a Windows banner on a Linux platform (*--dos* option) or a Linux banner under Windows (*--unicode* option).

Example:
![ansiShadow](./README_images/ansiShadow-01.png  "ansiShadow")

Accepted charaters are : "!#&(),-./0123456789:;< >?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[ ]^_ and space.
Unknown characters are converted to space.

## LICENSE
**ansiShadow** is covered by the GNU General Public License (GPL) version 2 and above.

## USAGE
``` bash
$ ansiShadow --help
ansiShadow - Copyright (c) 2017, Michel RIZZO. All Rights Reserved.
ansiShadow - Version 1.3.0

Text to ANSI Block Character Generator

Usage: ansiShadow [OPTIONS]...

  -h, --help                    Print help and exit
  -V, --version                 Print version and exit
  -u, --unicode                 Unicode output.  (default=off)
  -d, --dos                     DOS output.  (default=off)
  -s, --string=STRING           String to convert.
  -b, --background=BLACK|RED|GREEN|YELLOW|BLUE|MAGENTA|CYAN|WHITE
                                Background color

Exit: returns a non-zero status if an error is detected.

$ 
```
## STRUCTURE OF THE APPLICATION
This section walks you through **ansiShadow**'s structure. Once you understand this structure, you will easily find your way around in **ansiShadow**'s code base.

``` bash
$ yaTree
./                        # Application level
├── README_images/        # Images for documentation
│   └── ansiShadow-01.png # -- Screenshot ansiShadow
├── src/                  # Source directory
│   ├── Makefile          # -- Makefile
│   ├── ansiShadow.c      # -- C main source file
│   ├── ansiShadow.ggo    # -- 'gengetopt' option definition
│   │                     # -- Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   ├── dosChars.h        # -- C Header file with coding dor DOS
│   └── unicodeChars.h    # -- C Header file withcoding for UNICODE
├── COPYING.md            # GNU General Public License markdown file
├── LICENSE.md            # License markdown file
├── Makefile              # Top-level makefile
├── README.md             # ReadMe Mark-Down file
├── RELEASENOTES.md       # Release Notes markdown file
└── VERSION               # Version identification text file

2 directories, 12 files
$ 
```
## HOW TO BUILD THIS APPLICATION
``` bash
$ cd ansiShadow
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
``` bash
$ cd ansiShadow
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## SOFTWARE REQUIREMENTS
- For usage:
   - Nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
- Application developed and tested with  XUBUNTU 17.10.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES) .
***

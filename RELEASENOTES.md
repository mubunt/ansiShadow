# RELEASE NOTES: *ansiShadow*, an  ANSI Block Text Generator

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.3.14**:
  - Updated build system components.

- **Version 1.3.13**:
  - Updated build system.

- **Version 1.3.12**:
  - Removed unused files.

- **Version 1.3.11**:
  - Updated build system component(s)

- **Version 1.3.10**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.3.9**:
  - Some minor changes in .comment file(s).

- **Version 1.3.8**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.3.7**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.3.6:**
  - Added *git push* before *git push --tags*.

- **Version 1.3.5:**
  - Added tagging of new release.

- **Version 1.3.4:**
  - Moved from GPL v2 to GPL v3.
  - Replaced text files by markdown version: RELASENOTES, COPYING, LICENSE.
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.

- **Version 1.3.3:**
  - Fixed typos in README.md file.

- **Version 1.3.2:**
  - Fixed typo in RELEASE NOTES.

- **Version 1.3.1:**
  - Improved "clean" target in str/Makefile.
  - Formatting and improving the README file.

- **Version 1.3.0:**
  - Added background color option.

- **Version 1.2.0:**
  - Fixed trap of non managed characters.

- **Version 1.1.0:**
  - Added ".comment" file in each directory for 'yaTree' utility.

- **Version 1.0.0:**
  - First release.
